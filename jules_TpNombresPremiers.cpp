#include <iostream>
using namespace std;

bool nombrePremier(int nombre) {
    if (nombre != 2) {
        if (nombre < 2 || nombre % 2 == 0) {
            return false;
        }
        for (int i = 3; (i * i) <= nombre; i += 2) {
            if (nombre % i == 0) {
                return false;
            }
        }
    }
    return true;
}

int main()
{
    int nombre;

    cout << "Votre nombre est-il premier ? :" << endl;
    cin >> nombre;

    if (nombrePremier(nombre))
    {
        cout << "C'est un nombre premier" << endl;
    }
    else
    {
        cout << "Ce n'est pas un nombre premier" << endl;
    }
    return 0;
}